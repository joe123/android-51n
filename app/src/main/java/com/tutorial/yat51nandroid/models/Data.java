package com.tutorial.yat51nandroid.models;

import java.io.Serializable;

public class Data implements Serializable {
    public Data(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String name;
    public int age;
    public String gender;
}
