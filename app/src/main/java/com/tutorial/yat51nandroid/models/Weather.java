package com.tutorial.yat51nandroid.models;

public class Weather {
    public String cityName;
    public String description;
    public int temperature;

    public Weather(String cityName, String description, int temperature) {
        this.cityName = cityName;
        this.description = description;
        this.temperature = temperature;
    }
}
