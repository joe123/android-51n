package com.tutorial.yat51nandroid.models;

class Note {
    public String to = "Tove";
    public String from = "Jani";
    public String heading = "Reminder";
    public String body = "Don't forget me this weekend!";
}
