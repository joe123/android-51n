package com.tutorial.yat51nandroid.models;

//Customers
//CustomerID 	CustomerName 	ContactName 	Address 	City 	PostalCode 	Country
class Customers {
    public int CustomerID;
    public String CustomerName;
    public String ContactName;
    public String Address;
    public String City;
    public String PostalCode;
    public String Country;
}
