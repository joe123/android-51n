package com.tutorial.yat51nandroid.fragments;

import android.os.Bundle;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.tutorial.yat51nandroid.R;

public class PopMenuFragment extends Fragment {
    Button btn_popup;
    ImageView iv_mic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pop_menu, container, false);
        btn_popup = view.findViewById(R.id.btn_popup);
        iv_mic = view.findViewById(R.id.iv_mic);
        btn_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getActivity(), btn_popup);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popmenu, popup.getMenu());
                popup.show();
            }
        });
        return view;
    }
}