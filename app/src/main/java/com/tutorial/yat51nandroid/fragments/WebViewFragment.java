package com.tutorial.yat51nandroid.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.tutorial.yat51nandroid.R;

public class WebViewFragment extends Fragment {
    WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);
        webView = view.findViewById(R.id.webview);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.w3schools.com/");
//        webView.loadData("<!DOCTYPE html>\n" +
//                "<html>\n" +
//                "<body>\n" +
//                "\n" +
//                "<h2>An Unordered HTML List</h2>\n" +
//                "\n" +
//                "<ul>\n" +
//                "  <li>Coffee</li>\n" +
//                "  <li>Tea</li>\n" +
//                "  <li>Milk</li>\n" +
//                "</ul>  \n" +
//                "\n" +
//                "<h2>An Ordered HTML List</h2>\n" +
//                "\n" +
//                "<ol>\n" +
//                "  <li>Coffee</li>\n" +
//                "  <li>Tea</li>\n" +
//                "  <li>Milk</li>\n" +
//                "</ol> \n" +
//                "\n" +
//                "</body>\n" +
//                "</html>\n", "text/html", "UTF-8");
        return view;
    }
}