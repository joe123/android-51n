package com.tutorial.yat51nandroid.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tutorial.yat51nandroid.adapters.MyListAdapter;
import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.models.Weather;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewFragment extends Fragment {
    RecyclerView rv;
    List<Weather> weatherItems;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_view, container, false);
        rv = view.findViewById(R.id.rv);
        setDummyData();
        MyListAdapter adapter = new MyListAdapter(weatherItems);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
//        rv.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv.setAdapter(adapter);
        return view;
    }

    private void setDummyData() {
        weatherItems = new ArrayList<Weather>();
        Weather weather1 = new Weather("Berlin", "Snowy", 4);
        Weather weather2 = new Weather("Cairo", "Cold", 13);
        Weather weather3 = new Weather("Paris", "Sunny", 30);
        weatherItems.add(weather1);
        weatherItems.add(weather2);
        weatherItems.add(weather3);
    }
}