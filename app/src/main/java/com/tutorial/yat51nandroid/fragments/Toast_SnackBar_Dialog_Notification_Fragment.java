package com.tutorial.yat51nandroid.fragments;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.activities.SecondActivity;

import java.util.Calendar;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Toast_SnackBar_Dialog_Notification_Fragment extends Fragment {

    View view;
    Button btn_toast, btn_custom_toast,
            btn_snackbar,
            btn_custom_snackbar,
            btn_normal_dialog,
            btn_custom_dialog,
            btn_custom_dialogfragment,
            btn_datepicker,
            btn_timepicker,
            btn_progressbar,
            btn_progressdialog,
            btn_notifcaiton;
    DatePicker datePicker;
    TimePicker timePicker1;
    SeekBar seekBar;
    ProgressBar progressBar2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_toast__snack_bar__dialog__notification_, container, false);
        findViews();
        setClickListeners();
        datePickerFnc();
        timePickerFnc();
        seekBarFnc();
        progressBarFnc();
        return view;
    }

    private void progressBarFnc() {
        btn_progressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar2.setProgress(70);
            }
        });
    }

    private void seekBarFnc() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                showCustomToast("" + seekBar.getProgress());
            }
        });
    }

    private void timePickerFnc() {
        timePicker1.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                showCustomToast("hour : " + i + " Minute : " + i1);
            }
        });
    }

    private void datePickerFnc() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker.init(year, month, day,
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                        showCustomToast("" + day + " " + (month + 1) + " " + year);
                    }
                });
    }

    private void setClickListeners() {
        toast();
        customToast();
        snackBar();
        customSnackBar();
        normalDialog();
        customDialog();
        customDialogFragment();
        dataPickerDialog();
        timePickerDialog();
        progressDialogFnc();
        notificationFnc();
    }

    private void notificationFnc() {
        btn_notifcaiton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), SecondActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(getActivity(), (int) System.currentTimeMillis(), intent, 0);
                Notification n = new Notification.Builder(getActivity())
                        .setContentTitle("New mail from " + "test@gmail.com")
                        .setContentText("Subject")
                        .setSmallIcon(R.drawable.shopping_cart)
                        .setContentIntent(pIntent)
                        .setAutoCancel(true)
                        .addAction(R.drawable.ic_launcher_background, "And more", pIntent).build();
                NotificationManager notificationManager =
                        (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, n);
            }
        });
    }

    private void progressDialogFnc() {
        btn_progressdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle("ProgressDialog"); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(true);
            }
        });
    }

    private void timePickerDialog() {
        btn_timepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                showCustomToast("Hour :" + hourOfDay + " Minute : " + minute);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

    }

    private void dataPickerDialog() {
        btn_datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                showCustomToast("" + dayOfMonth + " " + (monthOfYear + 1) + " " + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    private void customDialogFragment() {
        btn_custom_dialogfragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogFragment customDialogFragment = new CustomDialogFragment();
                customDialogFragment.show(getChildFragmentManager(), "fragment");
            }
        });
    }

    AlertDialog alertDialog;

    private void customDialog() {
        btn_custom_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(false);
                View dialogView = LayoutInflater.from(
                        Toast_SnackBar_Dialog_Notification_Fragment.this.view.getContext())
                        .inflate(R.layout.custom_dialog1, null, false);
                Button button = dialogView.findViewById(R.id.buttonOk);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getActivity(), "Button inside pressed", Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                    }
                });
                builder.setView(dialogView);
                alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    private void normalDialog() {

        btn_normal_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Dialog body").setTitle("Dialog title")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(getActivity(), "you chose yes action for alertbox",
                                        Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                                Toast.makeText(getActivity(), "you chose no action for alertbox",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void toast() {
        btn_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Hello World", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void customToast() {
        btn_custom_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomToast("Hello world");

            }
        });
    }

    private void showCustomToast(String str) {
        LayoutInflater inf = getLayoutInflater();
        View layout = inf.inflate(R.layout.custom_toast,
                null);
        TextView tv = (TextView) layout.findViewById(R.id.tv_toast);
        tv.setText(str);
        Toast toast = new Toast(getActivity());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 100);
        toast.setView(layout);
        toast.show();
    }

    private void snackBar() {
        btn_snackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar
                        .make(Toast_SnackBar_Dialog_Notification_Fragment.this.view,
                                "www.journaldev.com",
                                Snackbar.LENGTH_LONG).
                        show();
            }
        });
    }

    private void customSnackBar() {
        btn_custom_snackbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Snackbar snackbar = Snackbar.make(Toast_SnackBar_Dialog_Notification_Fragment.this.view,
                        "", Snackbar.LENGTH_LONG);
                View customSnackView = getLayoutInflater().inflate(R.layout.custom_snackbar, null);
                snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
                Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                snackbarLayout.setPadding(0, 0, 0, 0);
                Button bGotoWebsite = customSnackView.findViewById(R.id.gotoWebsiteButton);
                bGotoWebsite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = "http://www.google.com";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        snackbar.dismiss();
                    }
                });
                snackbarLayout.addView(customSnackView, 0);
                snackbar.show();
            }
        });
    }

    private void findViews() {
        btn_toast = view.findViewById(R.id.btn_toast);
        btn_normal_dialog = view.findViewById(R.id.btn_normal_dialog);
        btn_custom_toast = view.findViewById(R.id.btn_custom_toast);
        btn_snackbar = view.findViewById(R.id.btn_snackbar);
        btn_custom_snackbar = view.findViewById(R.id.btn_custom_snackbar);
        btn_custom_dialog = view.findViewById(R.id.btn_custom_dialog);
        btn_custom_dialogfragment = view.findViewById(R.id.btn_custom_dialogfragment);
        datePicker = view.findViewById(R.id.datePicker1);
        btn_datepicker = view.findViewById(R.id.btn_datepicker);
        timePicker1 = view.findViewById(R.id.timePicker1);
        btn_timepicker = view.findViewById(R.id.btn_timepicker);
        seekBar = view.findViewById(R.id.seekBar);
        progressBar2 = view.findViewById(R.id.progressBar2);
        btn_progressbar = view.findViewById(R.id.btn_progressbar);
        btn_progressdialog = view.findViewById(R.id.btn_progressdialog);
        btn_notifcaiton = view.findViewById(R.id.btn_notifcaiton);
    }
}