package com.tutorial.yat51nandroid.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.yat51nandroid.database.AppDatabase;
import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.database.User;
import com.tutorial.yat51nandroid.database.UserDao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class StorageFragment extends Fragment {

    View view;
    Button btn_save_pref, btn_load_pref,
            btn_save_file, btn_load_file,
            btn_save_record, btn_load_records;
    TextView tv_pref, tv_file, tv_load_records;
    EditText ed_pref, et_file, et_firstname, et_lastname;
    static final String mypreference = "mypref";
    public static final String databaseName = "database-name";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_storage, container, false);
        findViews();
        setListeners();
        return view;
    }

    private void setListeners() {
        btn_save_pref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getActivity().getSharedPreferences(mypreference, MODE_PRIVATE); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("text", ed_pref.getText().toString());
                editor.commit();
            }
        });
        btn_load_pref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedpreferences = getActivity().getSharedPreferences(mypreference,
                        MODE_PRIVATE);
                String text = sharedpreferences.getString("text", "no text saved");
                tv_pref.setText(text);
            }
        });
        btn_save_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeFile();
            }
        });
        btn_load_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readFile();
            }
        });

        btn_save_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstname = et_firstname.getText().toString();
                String lastname = et_lastname.getText().toString();
                AppDatabase db = Room.databaseBuilder(getActivity(),
                        AppDatabase.class, databaseName).allowMainThreadQueries().build();
                UserDao userDao = db.userDao();
                User user = new User();
                user.first_name = firstname;
                user.last_name = lastname;
                userDao.insertAll(user);
                Toast.makeText(getActivity(), "New record added", Toast.LENGTH_LONG).show();
                et_firstname.setText("");
                et_lastname.setText("");
            }
        });

        btn_load_records.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppDatabase db = Room.databaseBuilder(getActivity(),
                        AppDatabase.class, databaseName).allowMainThreadQueries().build();
                UserDao userDao = db.userDao();
                List<User> users = userDao.getAll();
                String str = "";
                for (int i = 0; i < users.size(); i++) {
                    User user = users.get(i);
                    str += user.uid + " " + user.first_name + " " + user.last_name + '\n';
                }
                tv_load_records.setText(str);
            }
        });
    }

    private void findViews() {
        btn_save_pref = view.findViewById(R.id.btn_save_pref);
        btn_load_pref = view.findViewById(R.id.btn_load_pref);
        tv_pref = view.findViewById(R.id.tv_pref);
        ed_pref = view.findViewById(R.id.ed_pref);
        btn_save_file = view.findViewById(R.id.btn_save_file);
        btn_load_file = view.findViewById(R.id.btn_load_file);
        tv_file = view.findViewById(R.id.tv_file);
        et_file = view.findViewById(R.id.et_file);
        btn_save_record = view.findViewById(R.id.btn_save_record);
        btn_load_records = view.findViewById(R.id.btn_load_records);
        et_firstname = view.findViewById(R.id.et_firstname);
        et_lastname = view.findViewById(R.id.et_lastname);
        tv_load_records = view.findViewById(R.id.tv_load_records);
    }

    public void writeFile() {
        // add-write text into file
        try {
            FileOutputStream fileout = getActivity().openFileOutput("mytextfile.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(et_file.getText().toString());
            outputWriter.close();

            //display file saved message
            Toast.makeText(getActivity(), "File saved successfully!",
                    Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read text from file
    public void readFile() {
        //reading text from file
        int READ_BLOCK_SIZE = 100;
        try {
            FileInputStream fileIn = getActivity().openFileInput("mytextfile.txt");
            InputStreamReader inputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;
            while ((charRead = inputRead.read(inputBuffer)) > 0) {
                // char to string conversion
                String readstring = String.copyValueOf(inputBuffer, 0, charRead);
                s += readstring;
            }
            inputRead.close();
            tv_file.setText(s);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}