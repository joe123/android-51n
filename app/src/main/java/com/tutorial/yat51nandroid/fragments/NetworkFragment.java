package com.tutorial.yat51nandroid.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.network.APIClient;
import com.tutorial.yat51nandroid.network.Answer;
import com.tutorial.yat51nandroid.network.ListWrapper;
import com.tutorial.yat51nandroid.network.Question;
import com.tutorial.yat51nandroid.network.StackOverflowAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkFragment extends Fragment {

    View view;
    Button btn_call_api;
    TextView tv_api_response;
    EditText et_tag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_network, container, false);
        findViews();
        setClicks();
        return view;
    }

    private void setClicks() {
        btn_call_api.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });
    }

    private void callApi() {
        StackOverflowAPI api =
                APIClient.getClient().create(StackOverflowAPI.class);
        api.getQuestions(et_tag.getText().toString()).enqueue(new Callback<ListWrapper<Question>>() {
            @Override
            public void onResponse(Call<ListWrapper<Question>> call, Response<ListWrapper<Question>> response) {
                ListWrapper<Question> questions = response.body();
                String str = "";
                for (int i = 0; i < questions.items.size(); i++) {
                    Answer answer = questions.items.get(i);
                    str += answer.title + " " + answer.view_count + "\n";
                }
                tv_api_response.setText(str);

            }

            @Override
            public void onFailure(Call<ListWrapper<Question>> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void findViews() {
        btn_call_api = view.findViewById(R.id.btn_call_api);
        tv_api_response = view.findViewById(R.id.tv_api_response);
        et_tag = view.findViewById(R.id.et_tag);
    }
}