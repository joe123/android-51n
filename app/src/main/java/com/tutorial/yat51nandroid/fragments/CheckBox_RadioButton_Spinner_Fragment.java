package com.tutorial.yat51nandroid.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.tutorial.yat51nandroid.models.Data;
import com.tutorial.yat51nandroid.R;

public class CheckBox_RadioButton_Spinner_Fragment extends Fragment {
    Data data;
    CallBack callBack;
    TextView tv2;
    CheckBox checkBox;
    Button btn;
    View view;
    Button button;
    EditText editText;
    RadioGroup radioGroup;
    Spinner spinner;
    Button btn_callback;

    public CheckBox_RadioButton_Spinner_Fragment() {

    }

    public CheckBox_RadioButton_Spinner_Fragment(Data data) {
        this.data = data;
    }

    public void setOnCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_check_box__radio_button__spinner_, container, false);
        findViews();
        checkBoxFunction();
        editTextFunction();
        radioButtonFunction();
        spinnerFunction();
        tv2.setText(data.name + " " + data.age + " " + data.gender);
        btn_callback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.getData(tv2.getText().toString());
            }
        });
        return view;
    }

    private void findViews() {
        tv2 = view.findViewById(R.id.tv2);
        checkBox = view.findViewById(R.id.checkBox);
        btn = view.findViewById(R.id.button);
        button = view.findViewById(R.id.button2);
        editText = view.findViewById(R.id.editText);
        radioGroup = view.findViewById(R.id.radioGroup);
        spinner = view.findViewById(R.id.spinner);
        btn_callback = view.findViewById(R.id.btn_callback);
    }

    private void checkBoxFunction() {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
                checkBox.setChecked(true);
            }
        });
        checkBox.setText("Go home");
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked == true) {
                    tv2.setText("Check box is checked");
                } else {
                    tv2.setText("Check box is un--checked");
                }
            }
        });

    }

    private void radioButtonFunction() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = group.getCheckedRadioButtonId();
                RadioButton genderradioButton = (RadioButton) view.findViewById(selectedId);
                tv2.setText(genderradioButton.getText());
            }
        });

    }

    private void editTextFunction() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.setAlpha(0.5f);
                String str = editText.getText().toString();
                tv2.setText(str);
            }
        });
    }


    private void spinnerFunction() {
        String[] country = {"India", "USA", "China", "Japan", "Other"};
        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, country);
        spinner.setAdapter(aa);
    }


}