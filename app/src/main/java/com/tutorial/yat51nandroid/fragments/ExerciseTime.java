package com.tutorial.yat51nandroid.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.activities.ThirdActivity;

public class ExerciseTime extends Fragment {
    EditText editText;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exercise_time, container, false);
        button = view.findViewById(R.id.button9);
        editText = view.findViewById(R.id.editTextTextPersonName);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editText.getText().toString();
                Intent intent = new Intent(getActivity(), ThirdActivity.class);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        });
        return view;
    }
}