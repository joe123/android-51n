package com.tutorial.yat51nandroid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.tutorial.yat51nandroid.models.Data;
import com.tutorial.yat51nandroid.R;

public class SecondActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView textView = findViewById(R.id.textView2);
        Data data = (Data) getIntent().getSerializableExtra("data");
        if (data != null) {
            textView.setText(data.name + " " + data.age + " " + data.gender);
        }

    }
}