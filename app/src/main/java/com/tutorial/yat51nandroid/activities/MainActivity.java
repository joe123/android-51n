package com.tutorial.yat51nandroid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tutorial.yat51nandroid.fragments.CallBack;
import com.tutorial.yat51nandroid.models.Data;
import com.tutorial.yat51nandroid.fragments.ExerciseTime;
import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.fragments.CheckBox_RadioButton_Spinner_Fragment;
import com.tutorial.yat51nandroid.fragments.LocationFragment;
import com.tutorial.yat51nandroid.fragments.NetworkFragment;
import com.tutorial.yat51nandroid.fragments.PopMenuFragment;
import com.tutorial.yat51nandroid.fragments.RecyclerViewFragment;
import com.tutorial.yat51nandroid.fragments.StorageFragment;
import com.tutorial.yat51nandroid.fragments.Toast_SnackBar_Dialog_Notification_Fragment;
import com.tutorial.yat51nandroid.fragments.WebViewFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_checkbox_radio_spinner,
            btn_implicit_intent,
            btn_explicit_intent,
            btn_recycler_view,
            btn_exercise_time,
            btn_webview,
            btn_popupmenu,
            btn_toast_snackbar_dialog_notification,
            btn_storage,
            btn_network,
            btn_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findViews();
        setClickListeners();

    }

    private void findViews() {
        btn_checkbox_radio_spinner = findViewById(R.id.btn_checkbox_radio_spinner);
        btn_implicit_intent = findViewById(R.id.btn_implicit_intent);
        btn_explicit_intent = findViewById(R.id.btn_explicit_intent);
        btn_recycler_view = findViewById(R.id.btn_recycler_view);
        btn_exercise_time = findViewById(R.id.btn_exercise_time);
        btn_webview = findViewById(R.id.btn_webview);
        btn_popupmenu = findViewById(R.id.btn_popupmenu);
        btn_toast_snackbar_dialog_notification = findViewById(R.id.btn_toast_snackbar_dialog_notification);
        btn_storage = findViewById(R.id.btn_storage);
        btn_network = findViewById(R.id.btn_network);
        btn_location = findViewById(R.id.btn_location);
    }

    private void setClickListeners() {
        btn_checkbox_radio_spinner.setOnClickListener(this);
        btn_implicit_intent.setOnClickListener(this);
        btn_explicit_intent.setOnClickListener(this);
        btn_recycler_view.setOnClickListener(this);
        btn_exercise_time.setOnClickListener(this);
        btn_webview.setOnClickListener(this);
        btn_popupmenu.setOnClickListener(this);
        btn_toast_snackbar_dialog_notification.setOnClickListener(this);
        btn_storage.setOnClickListener(this);
        btn_network.setOnClickListener(this);
        btn_location.setOnClickListener(this);
    }


    public void navigateToFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, fragment).commit();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_checkbox_radio_spinner:
                CheckBox_RadioButton_Spinner_Fragment fragment = new CheckBox_RadioButton_Spinner_Fragment(new Data("youssef", 50, "male"));
                fragment.setOnCallBack(new CallBack() {
                    @Override
                    public void getData(String str) {
                        btn_checkbox_radio_spinner.setText(str);
                    }
                });
                navigateToFragment(fragment);
                break;
            case R.id.btn_implicit_intent:
                String phone = "+07775000";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            case R.id.btn_explicit_intent:
                Intent intent1 = new Intent(this, SecondActivity.class);
                Data data = new Data("youssef", 50, "male");
                intent1.putExtra("data", data);
                startActivity(intent1);
                break;
            case R.id.btn_recycler_view:
                navigateToFragment(new RecyclerViewFragment());
                break;
            case R.id.btn_exercise_time:
                navigateToFragment(new ExerciseTime());
                break;

            case R.id.btn_webview:
                btn_webview.setBackgroundColor(getResources().getColor(R.color.nebeti));
                navigateToFragment(new WebViewFragment());
                break;
            case R.id.btn_popupmenu:
                navigateToFragment(new PopMenuFragment());
                break;
            case R.id.btn_toast_snackbar_dialog_notification:
                navigateToFragment(new Toast_SnackBar_Dialog_Notification_Fragment());
                break;
            case R.id.btn_storage:
                navigateToFragment(new StorageFragment());
                break;
            case R.id.btn_network:
                navigateToFragment(new NetworkFragment());
                break;
            case R.id.btn_location:
                navigateToFragment(new LocationFragment());
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                Toast.makeText(this, "Exit selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            case R.id.action_settings:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }
}