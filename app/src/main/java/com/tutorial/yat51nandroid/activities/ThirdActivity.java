package com.tutorial.yat51nandroid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.tutorial.yat51nandroid.R;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        TextView textView = findViewById(R.id.textView3);
        String name = getIntent().getStringExtra("name");
        textView.setText(name);
    }
}