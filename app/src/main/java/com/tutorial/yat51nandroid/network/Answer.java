package com.tutorial.yat51nandroid.network;

import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("answer_id")
    public int answerId;
    public String title;
    //    @SerializedName("is_accepted")
//    public boolean accepted;
    public int view_count;

    public int score;

    public Owner owner;

}
