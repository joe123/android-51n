package com.tutorial.yat51nandroid.network;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface StackOverflowAPI {

    @GET("questions?order=desc&sort=votes&site=stackoverflow&filter=withbody")
    public Call<ListWrapper<Question>> getQuestions(@Query("tagged") String tagged);

    @GET("questions/{id}/answers?order=desc&sort=votes&site=stackoverflow")
    public Call<ListWrapper<Answer>> getAnswersForQuestion(@Path("id") String questionId);

}
