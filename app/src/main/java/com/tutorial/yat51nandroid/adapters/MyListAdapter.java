package com.tutorial.yat51nandroid.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tutorial.yat51nandroid.R;
import com.tutorial.yat51nandroid.models.Weather;

import java.util.List;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
    private List<Weather> weatherItems;
    public MyListAdapter(List<Weather> weatherItems) {
        this.weatherItems = weatherItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.viewholder_weather, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Weather weatherItem = weatherItems.get(position);
        holder.tv_city_name.setText(weatherItem.cityName);
        holder.tv_desc.setText(weatherItem.description);
        holder.tv_temp.setText(weatherItem.temperature + " C");
    }


    @Override
    public int getItemCount() {
        return weatherItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_city_name;
        public TextView tv_desc;
        public TextView tv_temp;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tv_city_name = (TextView) itemView.findViewById(R.id.tv_city_name);
            this.tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            this.tv_temp = (TextView) itemView.findViewById(R.id.tv_temp);
        }
    }
}
